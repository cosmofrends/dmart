const message = require("../constant/message");
const constant = require("../constant/constant");
const template = require("../constant/template");
const authUtil = require("../util/jwt.util");
const mailUtil = require("../util/mail.util");
const bcryptUtil = require("../util/bcrypt.util");
const { validationResult } = require("express-validator");
const userService = require("../service/user.service");
const {
  BadRequest,
  Ok,
  InternalServerError,
} = require("../util/response.util");

exports.loginUser = async (request, response) => {
  try {
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    let userDetail = await userService.findOneByEmail(body.email);
    if (!userDetail) {
      return BadRequest(response, {
        msg: message.emailInvalid,
        value: body.email,
      });
    }
    let compareBcryptFlag = await bcryptUtil.compareBcryptPassword(
      body.password,
      userDetail.toJson().password
    );
    if (!compareBcryptFlag) {
      return BadRequest(response, {
        msg: message.passwordInvalid,
        value: body.password,
      });
    }
    let token = await authUtil.createToken({
      userId: userDetail.toJson().id,
    });
    return Ok(response, message.loginSuccess, {
      tokenData: { token: token, type: "Bearer" },
      userData: {
        id: userDetail.id ? userDetail.id : null,
        firstName: userDetail.firstName ? userDetail.firstName : null,
        lastName: userDetail.lastName ? userDetail.lastName : null,
        email: userDetail.email ? userDetail.email : null,
        mobile: userDetail.mobile ? userDetail.mobile : null,
        attachment: userDetail.attachment ? userDetail.attachment : null,
        qr: userDetail.qr ? userDetail.qr : null,
        status: userDetail.status ? userDetail.status : null,
        createdAt: userDetail.createdAt ? userDetail.createdAt : null,
        updatedAt: userDetail.updatedAt ? userDetail.updatedAt : null,
      },
    });
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.logout = async (request, response) => {
  try {
    let params = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return response.status(400).send({
        code: 400,
        status: false,
        message: null,
        result: errors.array(),
      });
    }

    return response.status(200).send({
      code: 200,
      status: true,
      message: message.logoutSuccess,
      token: "",
      result: null,
    });
  } catch (error) {
    console.log(error);
    return response.status(500).send({
      code: 500,
      status: false,
      message: message.internalServerError,
      result: error,
    });
  }
};

exports.forgetUserPassword = async (request, response) => {
  try {
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    let emailDetail = await userService.findOneByEmail(body.email);
    if (!emailDetail) {
      return BadRequest(response, {
        msg: message.emailInvalid,
        value: body.email,
      });
    }
    let password = await bcryptUtil.generateRandomPasword(8);
    body.password = await bcryptUtil.bcryptPassword(password);

    let mailSubject = template.emailTemplate.forgetPasswordSubject;
    let mailBody = template.emailTemplate.forgetPasswordBody;
    let mailFooter = template.emailTemplate.footer;
    mailSubject = mailSubject.replace("<NEW_PASSWORD>", password);
    mailBody = mailBody.replace(
      "<NAME>",
      emailDetail.toJson().firstName + " " + emailDetail.toJson().lastName
    );
    mailBody = mailBody.replace("<EMAIL>", emailDetail.toJson().email);
    mailBody = mailBody.replace("<NEW_PASSWORD>", password);
    mailBody += " " + mailFooter;

    let mailData = new mailUtil.mailArg(
      emailDetail.toJson().email,
      mailSubject,
      mailBody
    );

    await mailUtil.sendMail(mailData, function (error, info) {
      if (error) {
        // console.log(error);
        return InternalServerError(response, error);
      }
      if (info)
        console.log(
          "Forget password email sent successfully, messageId => ",
          info.messageId
        );
    });
    await userService.update(body, emailDetail.toJson().id);
    return Ok(response, message.forgetPasswordSuccess);
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.changeUserPassword = async (request, response) => {
  try {
    let body = request.body;
    let loginUser = await authUtil.getAuthorizedUser(request);
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    let userId = loginUser && loginUser.id ? loginUser && loginUser.id : null;
    let userDetail = await userService.findOneById(userId);
    if (!userDetail) {
      return BadRequest(response, {
        msg: message.userIdInvalid,
        value: userId,
      });
    }
    let compareBcryptFlag = await bcryptUtil.compareBcryptPassword(
      body.oldPassword,
      userDetail.toJson().password
    );
    if (!compareBcryptFlag) {
      return BadRequest(response, {
        msg: message.oldPasswordInvalid,
        value: body.oldPassword,
      });
    }
    body.password = await bcryptUtil.bcryptPassword(body.newPassword);
    await userService.update(body, userId);
    return Ok(response, message.changePasswordSuccess, {
      id: userId,
    });
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.updateUserProfile = async (request, response) => {
  try {
    let body = request.body;
    let loginUser = await authUtil.getAuthorizedUser(request);
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    let userId = loginUser && loginUser.id ? loginUser && loginUser.id : null;
    let emailDetail = await userService.findOneByEmailAndNotId(
      body.email,
      userId
    );
    if (emailDetail) {
      return BadRequest(response, {
        msg: message.emailExists,
        value: body.email,
      });
    }
    body.updatedBy = userId;
    body.updatedAt = new Date();
    await userService.update(body, userId);
    let userDetail = await userService.findOneById(userId);
    if (!userDetail) {
      return BadRequest(response, {
        msg: message.userIdInvalid,
        value: userId,
      });
    }
    return Ok(response, message.profileUpdateSuccess, {
      id: userDetail.id ? userDetail.id : null,
      firstName: userDetail.firstName ? userDetail.firstName : null,
      lastName: userDetail.lastName ? userDetail.lastName : null,
      email: userDetail.email ? userDetail.email : null,
      mobile: userDetail.mobile ? userDetail.mobile : null,
      attachment: userDetail.attachment ? userDetail.attachment : null,
      qr: userDetail.qr ? userDetail.qr : null,
      status: userDetail.status ? userDetail.status : null,
      createdAt: userDetail.createdAt ? userDetail.createdAt : null,
      updatedAt: userDetail.updatedAt ? userDetail.updatedAt : null,
    });
  } catch (error) {
    return InternalServerError(response, error);
  }
};
