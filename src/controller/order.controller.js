const constant = require("../constant/constant");
const message = require("../constant/message");
const template = require("../constant/template");
const bcryptUtil = require("../util/bcrypt.util");
const { validationResult } = require("express-validator");
const {
  BadRequest,
  Ok,
  InternalServerError,
} = require("../util/response.util");
const { getAuthorizedUser } = require("../util/jwt.util");
const database = require("../models");
const service = require("../service/order.service");
const productService = require("../service/order-product.service");

exports.createOrder = async (request, response) => {
  const transaction = await database.getTransaction();
  try {
    let result = {};
    let loginUser = await getAuthorizedUser(request);
    let authId = loginUser && loginUser["id"] ? loginUser["id"] : null;
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    body["orderUniqueNo"] = await service.findLastUniqueId();
    result = await service.create(body, authId, transaction);
    if (result && body["products"]) {
      for (let item of body["products"]) {
        if (item) {
          item["ordersId"] = result["id"];
          await productService.create(item, authId, transaction);
        }
      }
    }
    await transaction.commit();
    return Ok(response, "Order created successfully", {
      id: result && result.id ? result.id : null,
    });
  } catch (error) {
    await transaction.rollback();
    return InternalServerError(response, error);
  }
};

exports.getOrderWithPagination = async (request, response) => {
  try {
    let result = {};
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    result = await service.getWithPagination(body);
    return Ok(response, "Order fetched successfully", result);
  } catch (error) {
    return InternalServerError(response, error);
  }
};
