const constant = require("../constant/constant");
const message = require("../constant/message");
const template = require("../constant/template");
const bcryptUtil = require("../util/bcrypt.util");
const { validationResult } = require("express-validator");
const {
  BadRequest,
  Ok,
  InternalServerError,
} = require("../util/response.util");
const { sendMail, mailArg } = require("../util/mail.util");
const { getAuthorizedUser } = require("../util/jwt.util");
const database = require("../models");
const userService = require("../service/user.service");

exports.createUser = async (request, response) => {
  const transaction = await database.getTransaction();
  try {
    let result = {};
    let loginUser = await getAuthorizedUser(request);
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    let emailDetail = await userService.findOneByEmail(body.email);
    if (emailDetail) {
      return BadRequest(response, {
        msg: message.emailExists,
        value: body.email,
      });
    }
    let password = await bcryptUtil.generateRandomPasword();
    body.password = await bcryptUtil.bcryptPassword(password);

    let mailSubject = template.emailTemplate.userCreateSubject;
    let mailBody = template.emailTemplate.userCreateBody;
    let mailFooter = template.emailTemplate.footer;
    mailBody = mailBody.replace("<NAME>", body.firstName + " " + body.lastName);
    mailBody = mailBody.replace("<EMAIL>", body.email);
    mailBody = mailBody.replace("<PASSWORD>", password);
    mailBody += " " + mailFooter;

    let mailData = new mailArg(body.email, mailSubject, mailBody);
    await sendMail(mailData, function (error, info) {
      if (error) {
        return InternalServerError(response, error);
      }
      if (info)
        console.log(
          "User created email sent successfully, messageId => ",
          info.messageId
        );
    });
    result = await userService.create(
      body,
      loginUser && loginUser.id ? loginUser && loginUser.id : null,
      transaction
    );
    await transaction.commit();
    return Ok(response, message.userCreateSuccess, {
      id: result && result.id ? result.id : null,
    });
  } catch (error) {
    await transaction.rollback();
    return InternalServerError(response, error);
  }
};

exports.updateUser = async (request, response) => {
  const transaction = await database.getTransaction();
  try {
    let loginUser = await getAuthorizedUser(request);
    let params = request.params;
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    let idDetail = await userService.findOneById(params.userId);
    if (!idDetail) {
      return BadRequest(response, {
        msg: message.userIdInvalid,
        value: params.userId,
      });
    }
    let emailDetail = await userService.findOneByEmailAndNotId(
      body.email,
      params.userId
    );
    if (emailDetail) {
      return BadRequest(response, {
        msg: message.emailExists,
        value: body.email,
      });
    }
    await userService.update(
      body,
      params.userId,
      loginUser && loginUser.id ? loginUser && loginUser.id : null
    );
    await transaction.commit();
    return Ok(response, message.userUpdateSuccess, { id: params.userId });
  } catch (error) {
    await transaction.rollback();
    return InternalServerError(response, error);
  }
};

exports.deleteUser = async (request, response) => {
  try {
    let loginUser = await getAuthorizedUser(request);
    let params = request.params;
    let idDetail = await userService.findOneById(params.userId);
    if (!idDetail) {
      return BadRequest(response, {
        msg: message.userIdInvalid,
        value: params.userId,
      });
    }
    await userService.remove(
      params.userId,
      loginUser && loginUser.id ? loginUser && loginUser.id : null
    );
    return Ok(response, message.userDeleteSuccess, { id: params.userId });
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.activeUser = async (request, response) => {
  try {
    let loginUser = await getAuthorizedUser(request);
    let params = request.params;
    let idDetail = await userService.findOneById(params.userId);
    if (!idDetail) {
      return BadRequest(response, {
        msg: message.userIdInvalid,
        value: params.userId,
      });
    }
    await userService.active(
      params.userId,
      loginUser && loginUser.id ? loginUser && loginUser.id : null
    );
    return Ok(response, message.userActiveSuccess, { id: params.userId });
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.inactiveUser = async (request, response) => {
  try {
    let loginUser = await getAuthorizedUser(request);
    let params = request.params;
    let idDetail = await userService.findOneById(params.userId);
    if (!idDetail) {
      return BadRequest(response, {
        msg: message.userIdInvalid,
        value: params.userId,
      });
    }
    await userService.inactive(
      params.userId,
      loginUser && loginUser.id ? loginUser && loginUser.id : null
    );
    return Ok(response, message.userInactiveSuccess, { id: params.userId });
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.getUserDetail = async (request, response) => {
  try {
    let params = request.params;
    let idDetail = await userService.findOneById(params.userId);
    return Ok(response, message.userFetchSuccess, idDetail ? idDetail : null);
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.getUserWithPagination = async (request, response) => {
  try {
    let result = {};
    let body = request.body;
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
      return BadRequest(response, errors.array());
    }
    result = await userService.getWithPagination(
      body.pageNumber,
      body.pageSize,
      body.keyword,
      body.sortColumn,
      body.sortOrder,
      body.status
    );
    return Ok(response, message.userFetchSuccess, result);
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.getAllUser = async (request, response) => {
  try {
    let result = [];
    result = await userService.findAll();
    return Ok(response, message.userFetchSuccess, result);
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.getAllActiveUser = async (request, response) => {
  try {
    let result = [];
    result = await userService.findAllActive();
    return Ok(response, message.userFetchSuccess, result);
  } catch (error) {
    return InternalServerError(response, error);
  }
};
