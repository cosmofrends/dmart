const config = require("../config");
const message = require("../constant/message");
const fs = require("fs");
const path = require("path");
const AWS = require("aws-sdk");
const multer = require("multer");
const multerS3 = require("multer-s3");
const {
  InternalServerError,
  BadRequest,
  Ok,
} = require("../util/response.util");

const storageType = config.storageType ? config.storageType : "LOCAL";
const publicDir = config.publicDir ? config.publicDir : "public/";
const uploadDir = config.uploadDir ? config.uploadDir : "upload/";
const storageS3Bucket = "";
const storageS3AccessKey = "";
const storageS3SecretKey = "";

exports.downloadFile = async (request, response) => {
  try {
    if (!request.query.path) {
      return BadRequest(response, {
        msg: "path fields is mandatory.",
        value: null,
        location: "query",
      });
    }
    if (storageType == "LOCAL") {
      var filePath = config.publicDir + request.query.path;
      var fileName = request.query.path;
      response.download(filePath, fileName);
    } else {
      var filePath = publicDir + request.query.path;
      var fileName = request.query.path;
      const s3 = new AWS.S3({
        accessKeyId: storageS3AccessKey,
        secretAccessKey: storageS3SecretKey,
      });
      const params = {
        Bucket: storageS3Bucket,
        Key: fileName,
      };
      s3.getObject(params, (error, data) => {
        if (error) console.error(error);
        // console.log(config.publicDir + fileName);
        fs.writeFileSync(publicDir + fileName, data.Body);
        response.download(filePath, fileName);
      });
    }
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.uploadSingleFile = async (request, response) => {
  try {
    const location = request.query.location ? request.query.location : "file";
    if (storageType == "LOCAL") {
      var dir = publicDir + uploadDir;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      dir += location;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      var upload = multer({
        storage: multer.diskStorage({
          destination: function (request, file, cb) {
            cb(null, publicDir);
          },
          filename: function (request, file, cb) {
            var extension = path.extname(file.originalname);
            var filename = "";
            if (location) {
              filename =
                uploadDir +
                location +
                "/" +
                location +
                "-" +
                Date.now() +
                extension;
            } else {
              filename = uploadDir + "/" + Date.now() + extension;
            }
            cb(null, filename);
          },
        }),
        fileFilter: function (request, file, callback) {
          var ext = path.extname(file.originalname);
          callback(null, true);
        },
      }).single("file");
      upload(request, response, function (error) {
        if (error) {
          console.log(error);
          if (error.code == "LIMIT_UNEXPECTED_FILE") {
            return BadRequest(response, {
              msg: "file fields is mandatory.",
              value: null,
            });
          }
          return InternalServerError(response, error);
        }
        return Ok(response, message.uploadFileSuccess, request.file);
      });
    } else {
      const s3 = new AWS.S3({
        accessKeyId: storageS3AccessKey,
        secretAccessKey: storageS3SecretKey,
      });
      var upload = multer({
        storage: multerS3({
          s3: s3,
          bucket: storageS3Bucket,
          metadata: function (request, file, cb) {
            cb(null, { fieldName: file.fieldname });
          },
          key: function (request, file, cb) {
            var ext = path.extname(file.originalname);
            cb(null, location + "/" + location + "-" + Date.now() + ext);
          },
        }),
        fileFilter: function (request, file, callback) {
          var ext = path.extname(file.originalname);
          callback(null, true);
        },
      }).single("file");
      upload(request, response, function (error) {
        if (error) {
          if (error.code == "LIMIT_UNEXPECTED_FILE") {
            return BadRequest(response, {
              msg: "file fields is mandatory.",
              value: null,
            });
          }
          return InternalServerError(response, error);
        }
        return Ok(response, message.uploadFileSuccess, request.file);
      });
    }
  } catch (error) {
    return InternalServerError(response, error);
  }
};

exports.uploadMultipleFile = async (request, response) => {
  try {
    const location = request.query.location ? request.query.location : "file";
    if (storageType == "LOCAL") {
      var dir = publicDir + uploadDir;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      dir += location;
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      var upload = multer({
        storage: multer.diskStorage({
          destination: function (request, file, cb) {
            cb(null, publicDir);
          },
          filename: function (request, file, cb) {
            var extension = path.extname(file.originalname);
            var filename = "";
            if (location) {
              filename =
                uploadDir +
                location +
                "/" +
                location +
                "-" +
                Date.now() +
                extension;
            } else {
              filename = uploadDir + "/" + Date.now() + extension;
            }
            cb(null, filename);
          },
        }),
        fileFilter: function (request, file, callback) {
          // var extension = path.extname(file.originalname);
          callback(null, true);
        },
      }).array("files");
      upload(request, response, function (error) {
        if (error) {
          if (error.code == "LIMIT_UNEXPECTED_FILE") {
            return BadRequest(response, {
              msg: "files fields is mandatory.",
              value: null,
            });
          }
          return InternalServerError(response, error);
        }
        return Ok(response, message.uploadFileSuccess, request.files);
      });
    } else {
      const s3 = new AWS.S3({
        accessKeyId: storageS3AccessKey,
        secretAccessKey: storageS3SecretKey,
      });
      var upload = multer({
        storage: multerS3({
          s3: s3,
          bucket: storageS3Bucket,
          metadata: function (request, file, cb) {
            cb(null, { fieldName: file.fieldname });
          },
          key: function (request, file, cb) {
            var extension = path.extname(file.originalname);
            var filename = "";
            if (request.query.location) {
              filename =
                location + "/" + location + "-" + Date.now() + extension;
            } else {
              filename = Date.now() + extension;
            }
            cb(null, filename);
          },
        }),
        fileFilter: function (request, file, callback) {
          // var extension = path.extname(file.originalname);
          callback(null, true);
        },
      }).array("files");
      upload(request, response, function (error) {
        if (error) {
          if (error.code == "LIMIT_UNEXPECTED_FILE") {
            return BadRequest(response, {
              msg: "files fields is mandatory.",
              value: null,
            });
          }
          return InternalServerError(response, error);
        }
        return Ok(response, message.uploadFileSuccess, request.files);
      });
    }
  } catch (error) {
    return InternalServerError(response, error);
  }
};
