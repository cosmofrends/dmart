const express = require("express");
const router = express.Router();
const passport = require("passport");
const validator = require("../validator/file.validator");
const fileController = require("../controller/file.controller");

router.get("/download", fileController.downloadFile);
router.post("/single-upload", fileController.uploadSingleFile);
router.post("/multiple-upload", fileController.uploadMultipleFile);

module.exports = router;
