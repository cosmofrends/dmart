const express = require("express");
const router = express.Router();
const validator = require("../validator/auth.validator");
const controller = require("../controller/auth.controller");
const jwtAuthMiddleware = require("../middleware/jwt-auth.passport");

router.post(
  "/login-user",
  validator.validate("loginUser"),
  controller.loginUser
);
router.put(
  "/logout",
  jwtAuthMiddleware,
  validator.validate("logout"),
  controller.logout
);
router.put(
  "/change-user-password",
  jwtAuthMiddleware,
  validator.validate("changeUserPassword"),
  controller.changeUserPassword
);
router.post(
  "/forget-user-password",
  validator.validate("forgetUserPassword"),
  controller.forgetUserPassword
);
router.put(
  "/update-user-profile",
  jwtAuthMiddleware,
  validator.validate("updateUserProfile"),
  controller.updateUserProfile
);

module.exports = router;
