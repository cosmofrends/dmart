const express = require("express");
const router = express.Router();
const fileRouter = require("./file.router");
const authRouter = require("./auth.router");
const userRouter = require("./user.router");
const orderRouter = require("./order.router");

router.get("/", (request, response) => {
  response.json({
    message: "Welcome to the Sadguru Bhojnalay API",
  });
});

router.use("/file", fileRouter);
router.use("/auth", authRouter);
router.use("/order", orderRouter);

module.exports = router;
