const express = require("express");
const router = express.Router();
const paginationValidator = require("../validator/pagination.validator");
const validator = require("../validator/order.validator");
const controller = require("../controller/order.controller");

router.post(
  "/create",
  // validator.validate("createOrder"),
  controller.createOrder
);
router.post(
  "/pagination",
  paginationValidator.validate("paginationDto"),
  controller.getOrderWithPagination
);

module.exports = router;
