const express = require("express");
const router = express.Router();
const validator = require("../validator/user.validator");
const controller = require("../controller/user.controller");
const paginationValidator = require("../validator/pagination.validator");
const jwtAuthMiddleware = require("../middleware/jwt-auth.passport");

router.post(
  "/default/create",
  validator.validate("userDto"),
  controller.createUser
);
router.post(
  "/create",
  jwtAuthMiddleware,
  validator.validate("userDto"),
  controller.createUser
);
router.put(
  "/update/:userId",
  jwtAuthMiddleware,
  validator.validate("userDto"),
  controller.updateUser
);
router.post(
  "/pagination",
  jwtAuthMiddleware,
  paginationValidator.validate("paginationDto"),
  controller.getUserWithPagination
);
router.get("/all", jwtAuthMiddleware, controller.getAllUser);
router.get("/active-all", jwtAuthMiddleware, controller.getAllActiveUser);
router.delete("/delete/:userId", jwtAuthMiddleware, controller.deleteUser);
router.put("/active/:userId", jwtAuthMiddleware, controller.activeUser);
router.put("/inactive/:userId", jwtAuthMiddleware, controller.inactiveUser);
router.get("/detail/:userId", jwtAuthMiddleware, controller.getUserDetail);
module.exports = router;
