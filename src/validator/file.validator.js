const { check, body, param, query } = require("express-validator");

exports.validate = (method) => {
  switch (method) {
    case "uploadSingleFile": {
      return [
        body("file")
          .exists()
          .withMessage("file is mandatory")
          .notEmpty()
          .withMessage("Provide file value"),
      ];
    }
    case "uploadMultipleFile": {
      return [
        body("files")
          .exists()
          .withMessage("files is mandatory")
          .notEmpty()
          .withMessage("Provide files value"),
      ];
    }
    case "download": {
      return [
        query("path")
          .exists()
          .withMessage("path is mandatory")
          .notEmpty()
          .withMessage("Provide path value"),
      ];
    }
  }
};
