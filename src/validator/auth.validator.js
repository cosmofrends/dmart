const { check, body, param, query } = require("express-validator");
const regrex = require("../constant/regrex");

exports.validate = (method) => {
  switch (method) {
    case "loginUser": {
      return [
        body("email")
          .exists()
          .withMessage("email is mandatory")
          .notEmpty()
          .withMessage("Provide email value")
          .isEmail()
          .withMessage("Provide valid email"),
        body("password")
          .exists()
          .withMessage("password is mandatory")
          .notEmpty()
          .withMessage("Provide password value"),
        body("deviceId")
          .optional()
          .notEmpty()
          .withMessage("Provide deviceId value"),
        body("deviceType")
          .optional()
          .notEmpty()
          .withMessage("Provide deviceType value")
          .isIn(["ANDROID", "IOS", "WEB"])
          .withMessage("Provide deviceType value as ANDROID or IOS or WEB"),
        body("fcmToken")
          .optional()
          .notEmpty()
          .withMessage("Provide fcmToken value"),
      ];
    }
    case "logout": {
      return [
        body("userId")
          .exists()
          .withMessage("userId is mandatory")
          .notEmpty()
          .withMessage("Provide userId value"),
        body("deviceId")
          .optional()
          .notEmpty()
          .withMessage("Provide deviceId value"),
        body("deviceType")
          .optional()
          .notEmpty()
          .withMessage("Provide deviceType value")
          .isIn(["ANDROID", "IOS", "WEB"])
          .withMessage("Provide deviceType value as ANDROID or IOS or WEB"),
      ];
    }
    case "forgetUserPassword": {
      return [
        body("email")
          .exists()
          .withMessage("email is mandatory")
          .notEmpty()
          .withMessage("Provide email value")
          .isEmail()
          .withMessage("Provide valid email"),
      ];
    }
    case "changeUserPassword": {
      return [
        body("oldPassword")
          .exists()
          .withMessage("oldPassword is mandatory")
          .notEmpty()
          .withMessage("Provide oldPassword value"),
        body("newPassword")
          .exists()
          .withMessage("newPassword is mandatory")
          .notEmpty()
          .withMessage("Provide newPassword value")
          .matches(regrex.passwordRegrex)
          .withMessage(
            "Please enter a password at least 8 character and contain At least one uppercase.At least one lower case.At least one special character."
          ),
      ];
    }
    case "updateUserProfile": {
      return [
        body("firstName")
          .exists()
          .withMessage("firstName is mandatory")
          .notEmpty()
          .withMessage("Provide firstName value")
          .isLength({ max: 50 })
          .withMessage("Provide firstName length upto 50 char"),
        body("lastName")
          .exists()
          .withMessage("lastName is mandatory")
          .notEmpty()
          .withMessage("Provide lastName value")
          .isLength({ max: 50 })
          .withMessage("Provide lastName length upto 50 char"),
        body("email")
          .exists()
          .withMessage("email is mandatory")
          .notEmpty()
          .withMessage("Provide email value")
          .isEmail()
          .withMessage("Provide valid email"),
        body("mobile")
          .optional()
          .notEmpty()
          .withMessage("Provide mobile value"),
        body("attachment")
          .optional()
          .notEmpty()
          .withMessage("Provide attachment value"),
      ];
    }
  }
};
