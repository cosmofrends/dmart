const { check, body, param, query } = require("express-validator");

exports.validate = (method) => {
  switch (method) {
    case "createOrder": {
      return [
        body("firstName")
          .exists()
          .withMessage("firstName is mandatory")
          .notEmpty()
          .withMessage("Provide firstName value")
          .isLength({ max: 50 })
          .withMessage("Provide firstName length upto 50 char"),
        body("lastName")
          .exists()
          .withMessage("lastName is mandatory")
          .notEmpty()
          .withMessage("Provide lastName value")
          .isLength({ max: 50 })
          .withMessage("Provide lastName length upto 50 char"),
        body("email")
          .exists()
          .withMessage("email is mandatory")
          .notEmpty()
          .withMessage("Provide email value")
          .isEmail()
          .withMessage("Provide valid email"),
        body("mobile")
          .exists()
          .withMessage("email is mandatory")
          .notEmpty()
          .withMessage("Provide email value"),
        body("address")
          .exists()
          .withMessage("address is mandatory")
          .notEmpty()
          .withMessage("Provide address value"),
        body("city")
          .exists()
          .withMessage("city is mandatory")
          .notEmpty()
          .withMessage("Provide city value"),
        body("pincode")
          .exists()
          .withMessage("pincode is mandatory")
          .notEmpty()
          .withMessage("Provide pincode value"),
        body("cardType")
          .exists()
          .withMessage("cardType is mandatory")
          .notEmpty()
          .withMessage("Provide cardType value")
          .isIn(["Credit Card", "Debit Card"])
          .withMessage("Only Credit Card or Debit Card allows"),
        body("cardData")
          .exists()
          .withMessage("cardData is mandatory")
          .notEmpty()
          .withMessage("Provide cardData value"),
        body("expiryData")
          .exists()
          .withMessage("expiryData is mandatory")
          .notEmpty()
          .withMessage("Provide expiryData value"),
        body("cvvData")
          .exists()
          .withMessage("cvvData is mandatory")
          .notEmpty()
          .withMessage("Provide cvvData value"),
        body("subTotal")
          .exists()
          .withMessage("subTotal is mandatory")
          .notEmpty()
          .withMessage("Provide subTotal value"),
        body("saved").exists().withMessage("saved is mandatory").optional(),
        body("total")
          .exists()
          .withMessage("total is mandatory")
          .notEmpty()
          .withMessage("Provide total value"),
        body("cashOnDeliveryStatus")
          .optional()
          .notEmpty()
          .withMessage("Provide cashOnDeliveryStatus value")
          .isIn(["1", "0"])
          .withMessage("Only 1 or 0 allows"),
      ];
    }
    case "getOrderWithPagination": {
      return [
        query("pageNumber")
          .exists()
          .withMessage("pageNumber is mandatory")
          .notEmpty()
          .withMessage("Provide pageNumber value"),
        query("pageSize")
          .exists()
          .withMessage("pageSize is mandatory")
          .notEmpty()
          .withMessage("Provide pageSize value"),
        query("keyword").exists().withMessage("keyword is mandatory"),
        query("sortColumn").exists().withMessage("sortColumn is mandatory"),
        query("sortOrder")
          .exists()
          .withMessage("sortOrder is mandatory")
          .isIn(["ASC", "DESC"])
          .withMessage("Provide sortOrder value as ASC or DESC"),
      ];
    }
    // default:
    //     return true;
  }
};
