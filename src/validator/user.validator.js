const { check, body, param, query } = require("express-validator");
const regrex = require("../constant/regrex");
const constant = require("../constant/constant");
const userService = require("../service/user.service");

exports.validate = (method) => {
  switch (method) {
    case "userDto": {
      return [
        body("firstName")
          .exists()
          .withMessage("firstName is mandatory")
          .notEmpty()
          .withMessage("Provide firstName value")
          .isLength({ max: 50 })
          .withMessage("Provide firstName length upto 50 char"),
        body("lastName")
          .exists()
          .withMessage("lastName is mandatory")
          .notEmpty()
          .withMessage("Provide lastName value")
          .isLength({ max: 50 })
          .withMessage("Provide lastName length upto 50 char"),
        body("email")
          .exists()
          .withMessage("email is mandatory")
          .notEmpty()
          .withMessage("Provide email value")
          .isEmail()
          .withMessage("Provide valid email"),
        body("mobile")
          .optional()
          .notEmpty()
          .withMessage("Provide mobile value"),
        body("attachment")
          .optional()
          .notEmpty()
          .withMessage("Provide attachment value"),
      ];
    }
    case "deleteUser": {
      return [
        // param("userId")
        //   .exists()
        //   .withMessage("userId is mandatory")
        //   .notEmpty()
        //   .withMessage("Provide userId value")
        //   .isNumeric()
        //   .withMessage("Provide valid userId value as number")
        //   .custom((userId, { request }) => {
        //     return userService.findOneById(userId).then((result) => {
        //       if (!result) {
        //         return Promise.reject("User not found");
        //       }
        //       if (
        //         result &&
        //         result.toJson().roleDetail.roleName != constant.ROLE_USER
        //       ) {
        //         return Promise.reject("Unauthorised request");
        //       }
        //     });
        //   }),
      ];
    }
    case "getUserDetail": {
      return [
        // param("userId")
        //   .exists()
        //   .withMessage("userId is mandatory")
        //   .notEmpty()
        //   .withMessage("Provide userId value")
        //   .isNumeric()
        //   .withMessage("Provide valid userId value as number")
        //   .custom((userId, { request }) => {
        //     return userService.findOneById(userId).then((result) => {
        //       if (!result) {
        //         return Promise.reject("User not found");
        //       }
        //       if (
        //         result &&
        //         result.toJson().roleDetail.roleName != constant.ROLE_USER
        //       ) {
        //         return Promise.reject("Unauthorised request");
        //       }
        //     });
        //   }),
      ];
    }
  }
};
