const { check, body, param, query } = require("express-validator");
const regrex = require("../constant/regrex");
const constant = require("../constant/constant");

exports.validate = (method) => {
  switch (method) {
    case "paginationDto": {
      return [
        body("pageNumber")
          .exists()
          .withMessage("pageNumber is mandatory")
          .notEmpty()
          .withMessage("Provide pageNumber value"),
        body("pageSize")
          .exists()
          .withMessage("pageSize is mandatory")
          .notEmpty()
          .withMessage("Provide pageSize value"),
        body("keyword").exists().withMessage("keyword is mandatory").optional(),
        body("sortColumn")
          .exists()
          .withMessage("sortColumn is mandatory")
          .optional(),
        body("sortOrder")
          .exists()
          .withMessage("sortOrder is mandatory")
          .optional()
          .isIn(["ASC", "DESC"])
          .withMessage("Provide sortOrder value as ASC or DESC"),
      ];
    }
  }
};
