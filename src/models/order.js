const constant = require("../constant/constant");

module.exports = (sequelize, DataTypes) => {
  const Orders = sequelize.define(
    "Orders",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      orderUniqueNo: DataTypes.STRING,
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: DataTypes.STRING,
      mobile: DataTypes.STRING,
      address: DataTypes.TEXT("long"),
      city: DataTypes.STRING,
      pincode: DataTypes.STRING,
      //Credit Card, Debit Card
      cardType: {
        type: DataTypes.STRING,
        defaultValue: "Debit Card",
      },
      nameOnCard: DataTypes.STRING,
      cardData: DataTypes.STRING,
      expiryData: DataTypes.STRING,
      cvvData: DataTypes.STRING,
      subTotal: DataTypes.STRING,
      saved: DataTypes.STRING,
      total: DataTypes.STRING,
      cardImage: DataTypes.TEXT("long"),
      cardName: DataTypes.STRING,
      cashOnDeliveryStatus: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
      },
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
    }
  );
  Orders.associate = (models) => {
    Orders.hasMany(models["OrderProduct"], {
      foreignKey: "ordersId",
      as: "products",
    });
  };
  Orders.prototype.toJson = function (pw) {
    let json = this.toJSON();
    return json;
  };
  return Orders;
};
