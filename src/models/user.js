const constant = require("../constant/constant");

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      firstName: DataTypes.STRING,
      lastName: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      mobile: DataTypes.STRING,
      attachment: {
        type: DataTypes.STRING,
        defaultValue: constant.USER_DEFAULT_ATTACHMENT_URL,
      },
      qr: DataTypes.STRING,
      // common field
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
      },
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
      deleteAt: DataTypes.DATE,
      deletedBy: DataTypes.INTEGER,
      activeAt: DataTypes.DATE,
      activeBy: DataTypes.INTEGER,
      inactiveAt: DataTypes.DATE,
      inactiveBy: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
    }
  );
  User.associate = (models) => {};
  User.prototype.toJson = function (pw) {
    let json = this.toJSON();
    return json;
  };
  return User;
};
