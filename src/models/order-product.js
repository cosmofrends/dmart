const constant = require("../constant/constant");

module.exports = (sequelize, DataTypes) => {
  const OrderProduct = sequelize.define(
    "OrderProduct",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      ordersId: DataTypes.INTEGER,
      productId: DataTypes.STRING,
      productName: DataTypes.TEXT("long"),
      productDesc: DataTypes.TEXT("long"),
      productImage: DataTypes.TEXT("long"),
      productVariant: DataTypes.STRING,
      previousPrice: DataTypes.STRING,
      price: DataTypes.STRING,
      discount: DataTypes.STRING,
      quantity: DataTypes.STRING,
      total: DataTypes.STRING,
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
      },
      createdBy: DataTypes.INTEGER,
      updatedBy: DataTypes.INTEGER,
    },
    {
      freezeTableName: true,
    }
  );
  OrderProduct.associate = (models) => {
    OrderProduct.belongsTo(models["Orders"], { foreignKey: "ordersId" });
  };
  OrderProduct.prototype.toJson = function (pw) {
    let json = this.toJSON();
    return json;
  };
  return OrderProduct;
};
