const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const basename = path.basename(__filename);
const config = require("../config");

const database = {};
const connection = new Sequelize(
  config.database ? config.database : "sadguru-bhojnalay",
  config.dbUsername ? config.dbUsername : "coderoot",
  config.dbPassword ? config.dbPassword : "cqSMpGaCIcUCUf3ok",
  {
    host: config.dbHost ? config.dbHost : "139.59.18.83",
    port: Number(config.dbPort ? config.dbPort : 21000),
    dialect: config.dialect ? config.dialect : "mysql",
    logging: false,
    pool: {
      max: 10,
      min: 0,
      idle: 10000,
    },
  }
);

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    var model = connection["import"](path.join(__dirname, file));
    database[model.name] = model;
  });

Object.keys(database).forEach((modelName) => {
  if (database[modelName].associate) {
    database[modelName].associate(database);
  }
});

database.connection = connection;
database.getTransaction = async () => {
  const transaction = await connection.transaction();
  return transaction;
};

module.exports = database;
