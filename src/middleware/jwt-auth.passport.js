const jwt = require("jsonwebtoken");
const config = require("../config");
const userService = require("../service/user.service");
const { UnAuthenticated } = require("../util/response.util");

const jwtAuthMiddleware = async (request, response, next) => {
  try {
    const authHeader = request.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];
    if (token == null) {
      return UnAuthenticated(response);
    }
    jwt.verify(token, config.tokenKey, async (error, jwt_payload) => {
      if (error) {
        console.error(error);
        return UnAuthenticated(response);
      }
      if (!jwt_payload || !jwt_payload.userId) {
        return UnAuthenticated(response);
      }
      let user = await userService.findOneById(jwt_payload.userId);
      if (!user) {
        return UnAuthenticated(response);
      }
      request.user = JSON.parse(JSON.stringify(user));
      next();
    });
  } catch (error) {
    console.error(error);
    return UnAuthenticated(response);
  }
};

module.exports = jwtAuthMiddleware;
