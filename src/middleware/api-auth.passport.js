const { ApiUnAuthenticated } = require("../util/response.util");

const apiAuthMiddleware = (request, response, next) => {
  let token = [
    "ilLNQ-Lcp_t5DBs9puJVI3JhwqlMndTILjkBrNd3Dsc",
    "_agec7UaYVN4c3RZQJQhUuR6nFSnqEXywv3QaIfFRFk",
    "3Uy7NIHilkOWviGXMRIl2ZUE4L7Mc8ub4VhosE3l8t8",
  ];
  try {
    let authorization = request.headers["api-authorization"];
    if (!authorization) {
      console.error("api auth token is blank or null or undefined");
      return ApiUnAuthenticated(response);
    } else {
      if (token.indexOf(authorization) > 0) {
        next();
      } else {
        console.error("Invalid api auth token");
        return ApiUnAuthenticated(response);
      }
    }
  } catch (error) {
    console.error(error);
    return ApiUnAuthenticated(response);
  }
};

module.exports = apiAuthMiddleware;
