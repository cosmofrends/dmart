module.exports = {
  // common
  internalServerErrorMsg: "Internal server error.",

  // file
  uploadFileSuccess: "File uploaded successfully.",
  uploadFileFailed: "File upload operation failed.",

  // auth
  emailInvalid: "Email is invalid.",
  passwordInvalid: "Password is invalid.",
  oldPasswordInvalid: "Old password is invalid.",
  unauthorizedLogin: "Unauthorized login.",
  loginSuccess: "Login successfully.",
  logoutSuccess: "Logout successfully.",
  forgetPasswordSuccess: "Password reset successfully.",
  changePasswordSuccess: "Password changed successfully.",
  profileUpdateSuccess: "Profile updated successfully.",

  // role
  roleNotFound: "Role not found.",
  roleNameExists: "Role name already exists.",
  roleCreateSuccess: "Role created successfully.",
  roleUpdateSuccess: "Role updated successfully.",
  roleDeleteSuccess: "Role deleted successfully.",
  roleFetchSuccess: "Role fetched successfully.",

  // user
  emailExists: "Email already exists.",
  userIdInvalid: "userId is invalid",
  userNotFound: "User not found.",
  userCreateSuccess: "User created successfully.",
  userUpdateSuccess: "User updated successfully.",
  userDeleteSuccess: "User deleted successfully.",
  userActiveSuccess: "User activeted successfully.",
  userInactiveSuccess: "User inactivated successfully.",
  userFetchSuccess: "User fetched successfully.",
};
