exports.emailTemplate = {
  footer: "<p>Thanks,</p><p>Demo.</p>",

  forgetPasswordSubject: "Forget Password - <NEW_PASSWORD>",
  forgetPasswordBody:
    "<p>Hi <NAME>,</p><p>Your password reset successfully. Your credential are belows.</p><p>Email: <EMAIL></p><p>New password: <NEW_PASSWORD></p>",

  userCreateSubject: "User created successfully",
  userCreateBody:
    "<p>Hi <NAME>,</p><p>Your account created successfully. Your credential are belows.</p><p>Email: <EMAIL></p><p>Password: <PASSWORD></p>",
};
