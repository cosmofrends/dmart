const config = require("../config");
const jwt = require("jsonwebtoken");

exports.createToken = (data) => {
  return (
    "Bearer " +
    jwt.sign(data, config.tokenKey, { expiresIn: config.tokenExpireTime })
  );
};

exports.getAuthorizedUser = (request) => {
  return request && request.user ? request.user : null;
};
