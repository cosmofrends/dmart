exports.isEmptyObject = (obj = {}) => {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
};

exports.isEmptyArray = (arr = []) => {
  return Array.isArray(arr) && !arr.length;
};

exports.isValidDateObject = (dateObject = {}) => {
  return new Date(dateObject).toString() !== "Invalid Date";
};

exports.isValidDateString = (dateString = "") => {
  return !isNaN(Date.parse(dateString));
};

exports.objectArrayHasKey = (arr = [], key = "") => {
  let object = {};
  if (key) {
    for (let index = 0; index < arr.length; index++) {
      const obj = arr[index];
      if (obj.hasOwnProperty(key)) {
        object.index = index;
        object.object = obj;
        return object;
      }
    }
  }
  return null;
};

exports.isObjectArrayHasKey = (arr = [], key = "") => {
  if (key) {
    for (const obj of arr) {
      if (obj.hasOwnProperty(key)) {
        return true;
      }
    }
  }
  return false;
};

exports.getAttrubutes = (attributes = [], alias = "") => {
  let str = "";
  for (const attr of attributes) {
    if (str) {
      str += ",";
    }
    if (alias) {
      str += " " + alias + ".";
    } else {
      str += " ";
    }
    str += attr;
  }
  return str;
};

exports.getPkResult = (resultset = []) => {
  if (Array.isArray(resultset) && resultset.length > 0) {
    return resultset[0];
  } else {
    return null;
  }
};

exports.createConstant = (str = "") => {
  // \w+ mean at least of one character that build word so it match every
  // word and function will be executed for every match
  var output = str
    .trim()
    .replace(/\w+/g, function (txt) {
      return txt.toUpperCase();
    })
    .replace(/\s/g, "_");
  return output;
};
