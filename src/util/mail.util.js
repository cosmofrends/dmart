const config = require("../config");
const nodemailer = require("nodemailer");

exports.mailArg = class mailArg {
  constructor(to, subject, message, attachment = []) {
    this.to = to ? to : null;
    this.subject = subject ? subject : null;
    this.message = message ? message : null;
    this.attachment = attachment && attachment.length > 0 ? attachment : [];
  }
};

exports.sendMail = async (mailArg, callback) => {
  let mailOptions = {
    from: config.mailFrom,
    to: mailArg.to ? mailArg.to : null,
    subject: mailArg.subject ? mailArg.subject : null,
    html: mailArg.message ? mailArg.message : null,
    attachments: mailArg.attachment ? mailArg.attachment : [],
  };
  const transporter = nodemailer.createTransport({
    host: config.mailHost,
    port: config.mailPort,
    secure: false,
    auth: {
      user: config.mailUsername,
      pass: config.mailPassword,
    },
    tls: { rejectUnauthorized: false },
  });
  await transporter.sendMail(mailOptions, callback);
};

// transporter.sendMail(mailOptions, (error, info) => {
//     if (error) {
//         return console.log(error);
//     }
//     console.log(info);
//     return info;
// });
