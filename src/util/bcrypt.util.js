const bcrypt = require("bcryptjs");

exports.bcryptPassword = (password) => {
  var hash = bcrypt.hashSync(password);
  return hash;
};

exports.compareBcryptPassword = async (password, hash) => {
  return await bcrypt.compare(password, hash);
};

exports.generateRandomPasword = (length = 8) => {
  let text = "";
  let possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
};
