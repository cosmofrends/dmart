// import { Logger } from "./logger";

const { info, error, internalError } = require("./logger.util");

exports.AppErrorCode = {
  /** Un-authenticated code. */
  UnAuthenticated: 1,

  /** Access denied or forbidden code. */
  Forbidden: 2,

  /** Internal server code. */
  InternalServerError: 3,

  /** The field is required code. */
  IsRequired: 4,

  /** The field type is invalid. */
  InvalidType: 5,

  /** The field type is String and its length is invalid. */
  InvalidLength: 6,

  /** The entity field value already exists in another entity. */
  ValueExists: 7,

  /** The entity can't be deleted due to its existing relations with other entities. */
  CantBeDeleted: 8,

  /**
   * The related entity isn't found,
   * @summary e.g. you are trying to create a new product in a category which is not exists in the database.
   */
  RelatedEntityNotFound: 9,

  // resource not found
  NotFound: 10,

  // resource is invalid
  Invalid: 11,

  // resource are same
  Same: 12,
};

/**
 * Returns a succeeded response with 200 status code.
 * @param response The http-response to be modified.
 * @param message An optional message that will be sent within the response' body.
 * @param body An optional body that will be sent within the response' body.
 */
exports.Ok = (response, message = "", body = null) => {
  info(message ? message : "");
  return response.send({
    status: true,
    msg: message ? message : null,
    result: body ? body : null,
  });
};

/**
 * Returns a bad-request response with 200 status code.
 * @param response The http-response to be modified.
 * @param body An optional body that will be sent within the response' body.
 */
exports.BadRequest = (response, body = null) => {
  error(body ? body : "");
  return body
    ? response.status(400).send({
        status: false,
        errors: body && Array.isArray(body) ? body : [body],
      })
    : response.status(400).send({ status: false });
};

/**
 * Returns an un-authenticated response with 401 status code.
 * @param response The http-response to be modified.
 */
exports.ApiUnAuthenticated = (response) => {
  error("Api token is not authenticated");
  return response.status(401).send({
    status: false,
    errors: [
      {
        // code: AppErrorCode.UnAuthenticated,
        msg: "Api token is not authenticated",
        // detail: "No valid access token provided",
      },
    ],
  });
};

/**
 * Returns an un-authenticated response with 401 status code.
 * @param response The http-response to be modified.
 */
exports.UnAuthenticated = (response) => {
  error("User is not authenticated");
  return response.status(401).send({
    status: false,
    errors: [
      {
        // code: AppErrorCode.UnAuthenticated,
        msg: "User is not authenticated",
        // detail: "No valid access token provided",
      },
    ],
  });
};

/**
 * Returns a forbidden response with 403 status code.
 * @param response The http-response to be modified.
 */
exports.Forbidden = (response) => {
  error("Access denied");
  return response.status(403).send({
    status: false,
    errors: [
      {
        // code: AppErrorCode.Forbidden,
        msg: "Access denied",
        // detail: `The user is trying to access a resource that he doesn't has the right to access`,
      },
    ],
  });
};

/**
 * Returns a notfound response with 404 status code.
 * @param response The http-response to be modified.
 * @param body An optional body that will be sent within the response' body.
 */
exports.NotFound = (response, body = null) => {
  error(body ? body : "");
  return body
    ? response.status(404).send({
        status: false,
        errors: body && Array.isArray(body) ? body : body ? body : [body],
      })
    : response.status(404).send({ status: false });
};

/**
 * Returns an internal server error response with 500 status code.
 * @param response The http-response to be modified.
 * @param error The error or error-message to be sent within the response' body.
 */
exports.InternalServerError = (response, error) => {
  internalError(
    "Internal server error => " + typeof error === "string"
      ? error
      : error.message
  );
  console.error(error);
  return response.status(500).send({
    status: false,
    errors: [
      {
        // code: AppErrorCode.InternalServerError,
        msg: "Internal server error",
        detail: typeof error === "string" ? error : error.message,
      },
    ],
  });
};
