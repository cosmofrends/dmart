const env = process.env.NODE_ENV || "development";
const config = require("../../config/config.json")[env.trim()];
module.exports = config;
