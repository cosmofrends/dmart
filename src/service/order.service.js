const Sequelize = require("sequelize");
const table = require("../constant/table");
const utility = require("../util/utility");
const { Orders, OrderProduct } = require("../models");

const attributes = [
  "id",
  "orderUniqueNo",
  "firstName",
  "lastName",
  "email",
  "mobile",
  "address",
  "city",
  "pincode",
  "cardType",
  "cardImage",
  "cardName",
  "nameOnCard",
  "cardData",
  "expiryData",
  "cvvData",
  "subTotal",
  "saved",
  "total",
  "cashOnDeliveryStatus",
  "createdAt",
];

exports.findLastUniqueId = async (logging = false) => {
  const result = await Orders.findOne({
    order: [["id", "DESC"]],
    logging: logging,
  });
  let uniqueNumber = "000001";
  if (result && result["orderUniqueNo"]) {
    let uniqueId = result["orderUniqueNo"].split("ORD".trim()).join("");
    let lastId = parseInt(uniqueId.trim());
    if (lastId < 10) {
      uniqueNumber = "00000" + (parseInt(lastId) + 1);
    } else if (lastId < 100 && lastId >= 10) {
      uniqueNumber = "0000" + (parseInt(lastId) + 1);
    } else if (lastId < 1000 && lastId >= 100) {
      uniqueNumber = "000" + (parseInt(lastId) + 1);
    } else if (lastId < 10000 && lastId >= 1000) {
      uniqueNumber = "00" + (parseInt(lastId) + 1);
    } else if (lastId < 100000 && lastId >= 10000) {
      uniqueNumber = "0" + (parseInt(lastId) + 1);
    }
  }
  return "ORD".trim() + uniqueNumber.trim();
};

exports.create = async (params = {}, createdBy = 0, transaction = null) => {
  return await Orders.create(
    {
      ...params,
      ...{
        createdBy: createdBy ? createdBy : undefined,
      },
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.update = async (
  params = {},
  id = 0,
  updatedBy = 0,
  transaction = null
) => {
  return await Orders.update(
    {
      ...params,
      ...{
        updatedBy: updatedBy ? updatedBy : undefined,
        updatedAt: new Date(),
      },
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.remove = async (id = 0, deletedBy = 0, transaction = null) => {
  return await Orders.update(
    {
      status: -1,
      deletedBy: deletedBy ? deletedBy : undefined,
      deleteAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.active = async (id = 0, activeBy = 0, transaction = null) => {
  return await Orders.update(
    {
      status: 1,
      activeBy: activeBy ? activeBy : undefined,
      activeAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.inactive = async (id = 0, inactiveBy = 0, transaction = null) => {
  return await Orders.update(
    {
      status: 0,
      inactiveBy: inactiveBy ? inactiveBy : undefined,
      inactiveAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.findOneById = async (id = 0) => {
  return await Orders.findOne({
    attributes: attributes,
    where: [{ id: id }],
  });
};

exports.findAll = async () => {
  return await Orders.findAll({
    attributes: attributes,
    where: { status: { [Sequelize.Op.ne]: -1 } },
  });
};

exports.findAllActive = async () => {
  return await Orders.findAll({
    attributes: attributes,
    where: { status: 1 },
  });
};

/*body.pageNumber,
body.pageSize,
  body.keyword,
  body.sortColumn,
  body.sortOrder,
  body.status,
  body.mobile,
  body.email;*/
exports.getWithPagination = async (pagination) => {
  let where = [];
  if (pagination["status"] > 0) {
    where.push({ status: pagination["status"] });
  }
  if (pagination["mobile"]) {
    where.push({ mobile: pagination["mobile"] });
  }
  if (pagination["email"]) {
    where.push({ email: pagination["email"] });
  }
  if (pagination["keyword"]) {
    where.push({
      [Sequelize.Op.or]: {
        orderUniqueNo: {
          [Sequelize.Op.iLike]: `%${pagination["keyword"]}%`,
        },
        firstName: {
          [Sequelize.Op.iLike]: `%${pagination["keyword"]}%`,
        },
        lastName: { [Sequelize.Op.iLike]: `%${pagination["keyword"]}%` },
      },
    });
  }
  let order = [];
  if (pagination.sortColumn && pagination.sortOrder) {
    order.push([pagination.sortColumn, pagination.sortOrder]);
  } else if (pagination.sortColumn) {
    order.push([pagination.sortColumn, "ASC"]);
  } else if (pagination.sortOrder) {
    order.push(["createdAt", pagination.sortOrder]);
  } else {
    order.push(["createdAt", "ASC"]);
  }
  if (pagination.pageSize && pagination.pageNumber) {
    let limit = Number(pagination.pageSize);
    let offset = (Number(pagination.pageNumber) - 1) * limit;
    let countResult = await Orders.findAndCountAll({
      distinct: true,
      include: [{ model: OrderProduct, as: "products" }],
      where: where,
      order: order,
      limit: limit,
      offset: offset,
    });
    if (countResult) {
      let result = {};
      result.totalRecord = 0;
      result.totalPage = 0;
      if (countResult.count) {
        let totalRecord = Number(countResult.count);
        let totalPage = totalRecord / limit;
        if (totalPage.toString().includes(".")) {
          var t = totalPage.toString().split(".");
          totalPage = Number(t[0].toString()) + 1;
        }
        result.totalRecord = totalRecord;
        result.totalPage = totalPage;
      }
      if (countResult.rows) {
        result.data = countResult.rows;
      }
      return result;
    }
    return null;
  } else if (pagination.pageSize) {
    let limit = Number(pagination.pageSize);
    return await Orders.findAll({
      where: where,
      order: order,
      limit: limit,
    });
  } else {
    return await Orders.findAll({
      where: where,
      order: order,
    });
  }
};
