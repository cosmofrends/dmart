const Sequelize = require("sequelize");
const table = require("../constant/table");
const utility = require("../util/utility");
const { OrderProduct } = require("../models");

const attributes = [
  "id",
  "ordersId",
  "productName",
  "price",
  "discount",
  "quantity",
  "total",
  "createdAt",
];

exports.deleteByOrdersId = async (ordersId = 0) => {
  return await OrderProduct.destroy({
    where: [{ ordersId: ordersId }],
  });
};

exports.create = async (params = {}, createdBy = 0, transaction = null) => {
  return await OrderProduct.create(
    {
      ...params,
      ...{
        createdBy: createdBy ? createdBy : undefined,
      },
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.update = async (
  params = {},
  id = 0,
  updatedBy = 0,
  transaction = null
) => {
  return await OrderProduct.update(
    {
      ...params,
      ...{
        updatedBy: updatedBy ? updatedBy : undefined,
        updatedAt: new Date(),
      },
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.remove = async (id = 0, deletedBy = 0, transaction = null) => {
  return await OrderProduct.update(
    {
      status: -1,
      deletedBy: deletedBy ? deletedBy : undefined,
      deleteAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.active = async (id = 0, activeBy = 0, transaction = null) => {
  return await OrderProduct.update(
    {
      status: 1,
      activeBy: activeBy ? activeBy : undefined,
      activeAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.inactive = async (id = 0, inactiveBy = 0, transaction = null) => {
  return await OrderProduct.update(
    {
      status: 0,
      inactiveBy: inactiveBy ? inactiveBy : undefined,
      inactiveAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.findOneById = async (id = 0) => {
  return await OrderProduct.findOne({
    attributes: attributes,
    where: [{ id: id }],
  });
};

exports.findAll = async () => {
  return await OrderProduct.findAll({
    attributes: attributes,
    where: { status: { [Sequelize.Op.ne]: -1 } },
  });
};

exports.findAllActive = async () => {
  return await OrderProduct.findAll({
    attributes: attributes,
    where: { status: 1 },
  });
};
