const Sequelize = require("sequelize");
const table = require("../constant/table");
const utility = require("../util/utility");
const { User } = require("../models");

const attributes = [
  "id",
  "firstName",
  "lastName",
  "email",
  "mobile",
  "password",
  "attachment",
  "qr",
  "status",
  "createdAt",
  "updatedAt",
];

exports.create = async (params = {}, createdBy = 0, transaction = null) => {
  return await User.create(
    {
      ...params,
      ...{
        createdBy: createdBy ? createdBy : undefined,
        activeBy: createdBy ? createdBy : undefined,
        activeAt: new Date(),
      },
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.update = async (
  params = {},
  id = 0,
  updatedBy = 0,
  transaction = null
) => {
  return await User.update(
    {
      ...params,
      ...{
        updatedBy: updatedBy ? updatedBy : undefined,
        updatedAt: new Date(),
      },
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.remove = async (id = 0, deletedBy = 0, transaction = null) => {
  return await User.update(
    {
      status: -1,
      deletedBy: deletedBy ? deletedBy : undefined,
      deleteAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.active = async (id = 0, activeBy = 0, transaction = null) => {
  return await User.update(
    {
      status: 1,
      activeBy: activeBy ? activeBy : undefined,
      activeAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.inactive = async (id = 0, inactiveBy = 0, transaction = null) => {
  return await User.update(
    {
      status: 0,
      inactiveBy: inactiveBy ? inactiveBy : undefined,
      inactiveAt: new Date(),
    },
    {
      where: [{ id: id }],
    },
    {
      transaction: transaction ? transaction : undefined,
    }
  );
};

exports.findOneById = async (id = 0) => {
  return await User.findOne({
    attributes: attributes,
    where: [{ id: id }],
  });
};

exports.findOneByEmail = async (email = "") => {
  return await User.findOne({
    where: [{ email: email, status: { [Sequelize.Op.ne]: -1 } }],
  });
};

exports.findOneByEmailAndNotId = async (email = "", id = 0) => {
  return await User.findOne({
    where: [
      { email: email },
      { status: { [Sequelize.Op.ne]: -1 } },
      { id: { [Sequelize.Op.ne]: id } },
    ],
  });
};

exports.findAll = async () => {
  return await User.findAll({
    attributes: attributes,
    where: { status: { [Sequelize.Op.ne]: -1 } },
  });
};

exports.findAllActive = async () => {
  return await User.findAll({
    attributes: attributes,
    where: { status: 1 },
  });
};

exports.getWithPagination = async (
  pageNumber = 1,
  pageSize = 10,
  keyword = "",
  sortColumn = "createdAt",
  sortOrder = "desc",
  status = 1
) => {
  let data = {};
  let result = [];
  let totalRecord = 0;
  let totalPage = 0;
  let isFirst = true;
  let attr = await utility.getAttrubutes(attributes, "u");
  let sql = "SELECT " + attr + " FROM " + table.user + " u";
  if (status >= 0) {
    if (isFirst) {
      sql += " WHERE ";
    } else {
      sql += " AND ";
    }
    isFirst = false;
    sql += " u.status = " + status;
  }
  if (keyword) {
    if (isFirst) {
      sql += " WHERE ";
    } else {
      sql += " AND ";
    }
    isFirst = false;
    sql += " (concat(u.firstName,' ',u.lastName) LIKE '%" + keyword + "%')";
  }
  if (sortColumn && sortOrder) {
    sql += " ORDER BY u." + sortColumn + " " + sortOrder;
  } else if (sortColumn) {
    sql += " ORDER BY u." + sortColumn + " DESC";
  } else if (sortOrder) {
    sql += " ORDER BY  u.createdAt" + sortOrder;
  } else {
    sql += " ORDER BY  u.createdAt DESC";
  }
  result = await User.sequelize.query(sql, {
    type: Sequelize.QueryTypes.SELECT,
  });
  totalRecord = result.length;
  totalPage = totalRecord / pageSize;
  if (totalPage.toString().includes(".")) {
    var t = totalPage.toString().split(".");
    totalPage = Number(t[0].toString()) + 1;
  }
  var offset = (pageNumber - 1) * pageSize;
  sql += " LIMIT  " + pageSize + " OFFSET " + offset;
  // console.log("sql => ", sql);
  result = await User.sequelize.query(sql, {
    type: Sequelize.QueryTypes.SELECT,
  });
  data.data = result;
  data.totalRecord = totalRecord;
  data.totalPage = totalPage;
  return data;
};
