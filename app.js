const express = require("express");
const app = express();
const http = require("http");
const cors = require("cors");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const parseError = require("parse-error");
const passport = require("passport");
const config = require("./src/config");
const database = require("./src/models");
const router = require("./src/router");
const apiAuthMiddleware = require("./src/middleware/api-auth.passport");
const { info } = require("./src/util/logger.util");

app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(config.publicDir));

app.use(passport.initialize());

app.use(morgan("dev"));

app.use("/api/v1", router);
app.get("/api/v1/health", async (req, res) => {
  return res.status(200).json({ status: "pong" });
});

app.get("/", (req, res) =>
  res.status(200).send({
    message: "Welcome to the API.",
  })
);

database.connection.sync().then(
  () => {
    // database.connection.query("SET FOREIGN_KEY_CHECKS = 0")
    // database.connection.sync({ alter: true });
    // database.connection.query("SET FOREIGN_KEY_CHECKS = 1");
  },
  (error) => {
    console.log(error);
  }
);

process.on("uncaughtException", function (e) {
  var data = parseError(e);
  console.error(data);
});

const port = config.port ? config.port : 3000;
app.set("port", port);
const server = http.createServer(app);
server.listen(port, () => {
  info(`Express server is running on port ${port}`, null, true);
  console.log(`Express server is running on port ${port}`);
});
module.exports = app;
