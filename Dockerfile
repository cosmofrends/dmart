FROM node:latest

RUN mkdir -p /dmart/

WORKDIR /dmart/

COPY package.json /dmart/

RUN npm install

COPY . /dmart/
EXPOSE 4000
CMD ["npm","run","start"]
